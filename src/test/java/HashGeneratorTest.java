import cloud.siebert.pva1.HashGenerator;
import cloud.siebert.pva1.ReadProperties;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class HashGeneratorTest {

    @Test
    public void TestCreatePasswordHash() {
        String testPassword = HashGenerator.createPasswordHash("Test");
        Assertions.assertTrue(testPassword.contains("$argon2id$v=19$m=262144,t=1,p=8$"));
    }

    @Test
    public void TestCompareHash() {
        Assertions.assertTrue(HashGenerator.compareHash("Test", "$argon2id$v=19$m=262144,t=1,p=8$/MRLHKqGVFzEG0l8BB+nqA$CcV8yoqEGrYZYa5Js08nEG5SVU4K+t+zZFAX8sSffCo"));
        Assertions.assertTrue(HashGenerator.compareHash("Foo", "$argon2id$v=19$m=262144,t=1,p=8$qscE1it3g23dwEnT5jxUww$6ASSGCBO8jd5oht/adrMDNHvUjfr2RjsH+85ItTUcVc"));
        Assertions.assertTrue(HashGenerator.compareHash("Kraftfahrzeug-Haftpflichtversicherung", "$argon2id$v=19$m=262144,t=1,p=8$LsJfwcJWB699SIlrIjVQnw$VDcDEgV3BjJxZyEv/XnFKJK21bpz5trL+jfemcFSDSk"));
    }

    @Test
    public void TestGetSecretKeyAES256() throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
        // Mocking ReadProperties method
        try (MockedStatic<ReadProperties> mockedReadProperties = Mockito.mockStatic(ReadProperties.class)) {
            Properties mockProps = new Properties();
            mockProps.setProperty("AESSecretKey", "testKey");
            mockProps.setProperty("AESSecretSalt", "testSalt");
            // Using mocked methods on call
            mockedReadProperties.when(() -> ReadProperties.readProperties("/pva1/config/config.properties")).thenReturn(mockProps);
            SecretKeySpec secretKey = HashGenerator.getSecretKeyAES256();
            assertNotNull(secretKey);
            Assertions.assertEquals(32, secretKey.getEncoded().length);
            Assertions.assertEquals("AES", secretKey.getAlgorithm());
        }
    }

    @Test
    public void testGetSecretIVSpecAES256() throws IOException {
        // Mocking ReadProperties method
        try (MockedStatic<ReadProperties> mockedReadProperties = Mockito.mockStatic(ReadProperties.class)) {
            Properties mockProps = new Properties();
            mockProps.setProperty("AESSecretIV", "testIV123");
            // Using mocked methods on call
            mockedReadProperties.when(() -> ReadProperties.readProperties("/pva1/config/config.properties")).thenReturn(mockProps);
            IvParameterSpec ivParameterSpec = HashGenerator.getSecretIVSpecAES256();
            assertNotNull(ivParameterSpec);
            Assertions.assertArrayEquals("testIV123".getBytes(), ivParameterSpec.getIV());
        }
    }
}