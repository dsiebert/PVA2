import cloud.siebert.pva1.ReadProperties;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ReadPropertiesTest {

    @Test
    void testReadPropertiesSuccess() throws IOException {
        // Create a temporary test properties file
        File tempFile = File.createTempFile("test", ".properties");
        try (FileWriter writer = new FileWriter(tempFile)) {
            writer.write("key1=value1\n");
            writer.write("key2=value2\n");
        }
        // Read the properties using the method under test
        Properties props = ReadProperties.readProperties(tempFile.getAbsolutePath());
        // Assert the expected values
        assertEquals("value1", props.getProperty("key1"));
        assertEquals("value2", props.getProperty("key2"));
        // Clean up the temporary file
        tempFile.delete();
    }

    @Test
    void testReadPropertiesFileNotFound() {
        String nonExistentFile = "non_existent_file.properties";
        assertThrows(IOException.class, () -> ReadProperties.readProperties(nonExistentFile));
    }
}